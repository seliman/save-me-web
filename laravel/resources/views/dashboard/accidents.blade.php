<meta charset="utf-8">
@extends('layouts.app')
@section('content')
  
  @if(Session::get('message','')!='')
    <div class="alert alert-{{Session::get('type','')}} alert-dismissible fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{Session::get('message','')}}
    </div>
  @endif
  {{Session::put('message','')}}

   @if(isset($message))
    <div class="alert alert-danger alert-dismissible fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{$message}}
    </div>
  @endif

     <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-car"></i> Accidents List</h3>
        </div>
        <div class="box-body">
        
       {!! Form::open(['action'=>'AccidentsController@store','method'=>'POST'])!!}

<div class="row">

            <div class="col-md-6">

              <div class="form-group">
                <label>From Date</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input id="from_date" class="form-control" type="text" value="{{$from_date}}" date-format="dd-mm-yyyy" name="from_date">
                </div>
             </div>  

            </div>
         
            <div class="col-md-6">

                 <div class="form-group">
                <label>To Date</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input id="to_date" class="form-control" type="text" value="{{$to_date}}" date-format="dd-mm-yyyy" name="to_date">
                </div>
            </div>  

            </div>

          </div>
 <button type="submit"  class="btn pull-right" style="margin-bottom: 10px; background-color: #083b66;color:#FFFFFF;">Search</button>
 
   <div class="clearfix"></div>
<table id="accidents_table" class="table table-bordered table-striped" >
                <thead>
                <tr>
                  <th>Driver Name</th>
                  <th>City</th>
                  <th>Country</th>
                  <th>Date</th>
                  <th>Day</th>
                  <th>Time</th>
                  <th>Weather</th>
                  <th>Speed</th>
                  <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                
                  @if(count($accidents)>1)
            @foreach($accidents as $accident)
              <tr>
                <td>{{$accident->First_name}} {{$accident->Middle_name}} {{$accident->Last_name}}</td>
                <td>{{utf8_decode($accident->city)}}</td>
                <td>{{utf8_decode($accident->country)}}</td>
                <td>{{$accident->Accident_date}}</td>
                <td>{{$accident->dayname}}</td>
                <td>{{$accident->Accident_time}}</td>
                <td>{{$accident->weather}}</td>
                <td>{{$accident->Speed_limit}}</td>
                <td><div class="text-center"><a href="edit/{{$accident->idAccident_info}}" class="btn btn-social-icon btn-bitbucket"><i class="fa fa-edit"></i></a> </div></td>
              </tr>
            @endforeach
          @endif
                
                  
                

                </tbody>
              </table>
{!! Form::close() !!} 
            </div>
            <!-- /.box-body -->
          </div>
          
@endsection

@section('scripts')

<script>
    sidemenu_active(1);
   // $('#accidents_table').DataTable();
    $('#accidents_table').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })

$('#from_date').datepicker({
      autoclose: true,
      format: 'd-m-yyyy',
    })

$('#to_date').datepicker({
      autoclose: true,
      format: 'd-m-yyyy',
    })

</script>

@endsection