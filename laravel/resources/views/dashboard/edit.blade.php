@extends('layouts.app')
@section('content')

 
@if(Session::get('message','')!='')
    <div class="alert alert-{{Session::get('type','')}} alert-dismissible fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{Session::get('message','')}}
    </div>
@endif
{{Session::put('message','')}}

 @if(isset($message))
    <div class="alert alert-{{$type}} alert-dismissible fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{$message}}
    </div>
  @endif

     <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-car"></i> Accidents List</h3>
        </div>
        <div class="box-body">
        
        {!! Form::open(['route' => ["{accident_id}.store",$accident[0]->idAccident_info],'action'=>'EditsController@store','method'=>'POST'])!!}
           <!-- <p>{{$accident[0]->idAccident_info}}</p>  -->
             
           <div class="form-group">
                <label>Accident Date</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input id="date_picker" class="form-control" type="text" value="{{$accident[0]->Accident_date}}"  date-format="dd-mm-yyyy" name="accident_date">
                </div>
            </div>


            <div class="form-group">
                <label>Accident Day</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <select class="form-control" id="exampleSelect1" name="accident_day">
                  	@foreach($day_week as $day)
                  		@if($accident[0]->Day_of_Week==$day->idDayWeek)
				      	<option selected="selected" value="{{$day->idDayWeek}}" >{{$day->name}}</option>
				      	@else
				      	<option value="{{$day->idDayWeek}}" >{{$day->name}}</option>
				      	@endif
				    @endforeach 
			      </select>
                </div>
            </div>

            <div class="form-group">
                <label>Accident Time</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa  fa-clock-o"></i>
                  </div>
                  <input class="form-control" type="time" value="{{$accident[0]->Accident_time}}" id="example-time-input" name="accident_time">
                </div>
            </div>

			<div class="form-group">
                <label>Light Condition</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa  fa-lightbulb-o"></i>
                  </div>
                  <select class="form-control" id="exampleSelect1" name="light_condition">
                  	@foreach($light_condition as $light)
                    @if($accident[0]->Light_condition==$light->idLight)
                    <option selected="selected" value="{{$light->idLight}}" >{{$light->name}}</option>
                    @else
                    <option value="{{$light->idLight}}" >{{$light->name}}</option>
                    @endif
				        @endforeach 
			      </select>
                </div>
            </div>


            <div class="form-group">
                <label>Weather Condition</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-sun-o"></i>
                  </div>
                  <select class="form-control" id="exampleSelect1" name="weather_condition">
                  	@foreach($weather_condition as $weather)
				      	    @if($accident[0]->Weather_condition==$weather->idWeather)
                    <option selected="selected" value="{{$weather->idWeather}}" >{{$weather->name}}</option>
                    @else
                    <option value="{{$weather->idWeather}}" >{{$weather->name}}</option>
                    @endif
				    @endforeach 
			      </select>
                </div>
            </div>

           <div class="form-group">
                <label>Speed Limit(km/s)</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-tachometer"></i>
                  </div>
                  <input class="form-control" type="text" value="{{$accident[0]->Speed_limit}}" name="speed_limit">
                </div>
            </div>

            

		   <button type="submit"  class="btn pull-right" style="background-color: #083b66;color:#FFFFFF;">Save Changes</button>
        
        {!! Form::close() !!} 

            </div>
          </div>

         <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-list"></i> List of Victims</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                <tr>
                  <th>Name</th>
                  <th>Victim Category</th>
                  <th>Injury Severity</th>
                  <th>Hospital Name</th>
                </tr>
                @foreach($victims as $victim)

	                <tr>
	                  <td>
	                  	@if($victim->victim_name=='')
	                  		{{$victim->First_name}} {{$victim->Middle_name}} {{$victim->Last_name}}
	                  	@else
	                  		{{$victim->victim_name}}
	                  	@endif
	                  </td>
	                  <td>{{$victim->victim_type}}</td>
	                  <td>{{$victim->injury_type}}</td>
	                  <td>{{$victim->hospital_name}}</td>
	                </tr>
	                
                @endforeach
              </tbody>
          	</table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div> 
          
@endsection

@section('scripts')

<script>
 
  $('#date_picker').datepicker({
      autoclose: true,
      format: 'd-m-yyyy',
    })


</script>

@endsection