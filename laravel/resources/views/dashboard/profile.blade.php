@extends('layouts.app')
@section('content')

@if(Session::get('message','')!='')
    <div class="alert alert-{{Session::get('type','')}} alert-dismissible fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{Session::get('message','')}}
    </div>
@endif
{{Session::put('message','')}}

 <!-- @if(isset($message))
    <div class="alert alert-{{$type}} alert-dismissible fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{$message}}
    </div>
  @endif -->

     <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-car"></i> Profile</h3>
        </div>
        <div class="box-body">
        
        {!! Form::open(['action'=>'ProfilesController@store','method'=>'POST'])!!}

           <div class="form-group">
                <label>Username</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-car"></i>
                  </div>
                  <input class="form-control" type="text" id="username" name="username" value="{{$profile[0]->userName}}">
                </div>
            </div>

            <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <p type="text" class="form-control">Change Password</p>
                <span class="input-group-addon" data-toggle="collapse" data-target="#demo"><i class="fa fa-angle-down"></i></span>
              </div>
            </div>

               <div id="demo" class="collapse">
                <div class="box-body">
                  <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-lock"></i>
                          </div>
                          <input class="form-control input-sm" type="Password" name="old_password" id="old_password" placeholder="Old Password">
                        </div>
                </div>

                  <div class="form-group">
                          <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-lock"></i>
                          </div>
                          <input class="form-control input-sm" type="Password" id="new_password" name="new_password" placeholder="New Password">
                        </div>
                  </div>
                    <div class="form-group">
                          <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-lock"></i>
                          </div>
                          <input class="form-control input-sm" type="Password" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
                        </div>
                      </div>  
               </div>
               </div>

            <div class="form-group">
                <label>Email</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                  </div>
                  <input class="form-control" type="text" name="email" value="{{$profile[0]->Email}}">
                </div>
            </div>

            <div class="form-group">
                <label>Role</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-suitcase"></i>
                  </div>
                  <input class="form-control" type="text" name="role" value="{{$profile[0]->role}}">
                </div>
            </div>

            <input type="hidden" name="username_changed" id="username_changed" value="0">            

       <button type="submit"  class="btn pull-right" style="background-color: #083b66;color:#FFFFFF;">Save Changes</button>
        
        {!! Form::close() !!} 

            </div>
          </div>
            
          
@endsection

@section('scripts')

<script>
$("#username").on('change',function()
{
  $("#username_changed").val(1);
});
</script>

@endsection
