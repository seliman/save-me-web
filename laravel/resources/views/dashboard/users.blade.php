@extends('layouts.app')
 
@section('content')


@if(Session::get('message','')!='')
    <div class="alert alert-{{Session::get('type','')}} alert-dismissible fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{Session::get('message','')}}
    </div>
@endif
{{Session::put('message','')}}


<div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-list"></i> List Users</h3>
        </div>
        <div class="box-body">

<table id="users_table" class="table table-bordered table-striped" >
                <thead>
                <tr>
                  <th>Full Name</th>
                  <th>Username</th>
                  <th>Phone Number</th>
                  <th>Email</th>
                  <!-- <th>Address</th> -->
                  <th>User Type</th>
                  <th>Hardware</th>
                  <th>Active</th>
                </tr>
                </thead>
                <tbody>
                
                  @if(count($users)>1)
			    	@foreach($users as $user)
			    		<tr>
			    			<td>{{$user->First_name}} {{$user->Middle_name}} {{$user->Last_name}}</td>
			    			<td>{{$user->userName}}</td>
							<td>{{$user->PhoneNumber}}</td>
			    			<td>{{$user->Email}}</td>
			    			<!-- <td>{{$user->driver_address}}</td> -->

                <td>
                  <span class="label label-{{$user_type[$user->idUserType]}}">{{$user->type}}</span>
                </td>

			    			<td>
                  @if($user->userType!=1)
                     <input  type="checkbox" data-on="Enable" data-off="Disable" disabled  data-toggle="toggle"/>
                  @else
                    @if($user->use_hardware ==1)
                     <input  type="checkbox" data-on="Enable" data-off="Disable" checked  data-toggle="toggle" id="use_hardware_{{$user->idDriver}}" onchange="user_toggle('use_hardware','{{$user->idDriver}}','1')"/>
                    @else
                      <input  type="checkbox" data-on="Enable" data-off="Disable"   data-toggle="toggle" id="use_hardware_{{$user->idDriver}}" onchange="user_toggle('use_hardware','{{$user->idDriver}}','1')"/>
                    @endif
                  @endif
                </td>

			    			<td>
                  @if($user->active ==1)
                    <input  type="checkbox" data-on="Active" data-off="Deactive" checked data-toggle="toggle" id="active_{{$user->id}}" onchange="user_toggle('active','{{$user->id}}','2')"/>
                  @else
                    <input  type="checkbox" data-on="Active" data-off="Deactive" data-toggle="toggle" id="active_{{$user->id}}" onchange="user_toggle('active','{{$user->id}}','2')"/>
                  @endif
                </td>
			    			
			    		</tr>
			    	@endforeach
			    @endif

                </tbody>
              </table>
        <!-- /.box-body -->
      </div>

@endsection

  
@section('scripts')

  
        <script>

sidemenu_active(3);
//$('#users_table').DataTable();

    $('#users_table').DataTable({
      'paging'      : false,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
 
//$("#hardware_1").bootstrapToggle('on');
function user_toggle(key,id,type)
{
	var is_checked = ($("#"+key+"_"+id).is(":checked"))?1:0;

	stopLoading();
  //alert(is_checked +"  "+key);
  //http://192.168.1.79:8080/ionic/user_toggle.php
  request('http://192.168.1.79:8080/ionic/user_toggle.php','id='+id+'&key='+key+'&value='+is_checked+'&type='+type,function(data)
  {
    if(data['success']==0)
    {
      alert('An Error Occurred');
    }

  });

}
</script>

     
@endsection
