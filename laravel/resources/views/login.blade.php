
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SaveMe</title>
      

  <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="{{asset('css/waitMe.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/really-simple-jquery-dialog.css')}}">
</head>
<body class="hold-transition login-page" id="body">
<div class="login-box">
  <div class="login-logo">
    <span><b>Save</b>Me</span>

 
  </div>
  <!-- /.login-logo -->
  <form method="POST">
  <div class="login-box-body">
    <p class="login-box-msg">Login</p>

	<div class="social-auth-links text-center">
	     <img src="{{asset('logo.png')}}">
    </div>
     {!! Form::open(['action'=>'LoginsController@store','method'=>'POST'])!!} 
   
   @if(isset($error_message))
      
  <div class="alert alert-danger alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{$error_message}}
  </div>

 
      @endif
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
<!--             <label> -->
<!--               <input type="checkbox"> Remember Me -->
<!--             </label> -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-default btn-block btn-flat" id="signin" onhober style="background-color: #083b66;color:#FFFFFF;"  >Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    {!! Form::close() !!} 


<!--     <a href="#">I forgot my password</a><br> -->
<!--     <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
	  
<div id="testConfirm"></div>
<div id="testAlert"></div>
<div id="testPrompt"></div>

<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('js/waitMe.js')}}"></script>
<script src="{{asset('js/config.js')}}"></script>
<script src="{{asset('js/really-simple-jquery-dialog.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });




$("#signin").on('click',function()
{
  /*var username = 'admin';
  var password = 'admin';
	request('http://savemeapp.co/appServices/login.php','username='+username+'&password='+password,function(data){
		if(data['success'] == 1)
    {
      <?php
      // session_start();
      // $_SESSION['islogin']=1;
      ?>
      window.location = '/accidents';
    }
    else{
      alert("Invalid Credentials, kindly Check Email/Password correctness");
    }
		})*/
});
  

</script>
</body>
</html>



