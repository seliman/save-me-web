<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class LoginsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$requests = $request->all();
        $username= $requests['username'];
        $password = $requests['password'];
        $logins =DB::select("SELECT * FROM user u LEFT JOIN admin_role ar ON ar.user_id=u.userId  where username='".$username."'and password='".md5($password)."' AND userType=5");
        if(count($logins)>=1)
        {
            session_start();
            $_SESSION['islogin']=1;
            $_SESSION['username']= $logins[0]->userName;
            $_SESSION['user_id']= $logins[0]->userId;
            $_SESSION['email']= $logins[0]->Email;
            $_SESSION['role']= $logins[0]->role;
            $_SESSION['created']= $logins[0]->created;
            $request->session()->put('user_id', $logins[0]->userId);
            $request->session()->put('admin_username', $logins[0]->userName);
            $request->session()->put('admin_email', $logins[0]->Email);
            $request->session()->put('admin_role', $logins[0]->role);
            return redirect('accidents');
        }
        else{
         return view('login')->with('error_message','Invalid Credentials');

        }

        //return $requests['username'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  view$id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

