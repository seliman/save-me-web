<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

class AccidentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $year = date('Y');

        $month = date('m');
        $month = (substr($month,0,1) == "0")?substr($month,1,1):$month;

        $day = date('d');
        $day = (substr($day,0,1) == "0")?substr($day,1,1):$day;

        $to_date = $day."-".$month."-".$year;

        $year = date('Y');

        $month = date('m')-1;
        $month = (substr($month,0,1) == "0")?substr($month,1,1):$month;

        $day = date('d');
        $day = (substr($day,0,1) == "0")?substr($day,1,1):$day;

        $from_date = $day."-".$month."-".$year;


        $month = date('m');
        $month = (substr($month,0,1) == "0")?substr($month,1,1):$month;

         /*$query = "SELECT *,wc.name as weather , lc.name as light,dw.name as dayname,count(*) FROM accident_info ai ";
         $query .= " LEFT JOIN accident_location al ON ai.idAccident_location=al.idAccident_location";
         $query .= " LEFT JOIN day_week dw ON dw.idDayWeek=ai.Day_of_Week";
         $query .= " LEFT JOIN light_condition lc ON lc.idLight = ai.Light_condition";
         $query .= " LEFT JOIN weather_condition wc ON wc.idWeather=ai.Weather_condition";
         $query .= " LEFT JOIN victim v ON v.Accident_Info_idAccident_Info=ai.idAccident_info";
         $query .= " LEFT JOIN driver d ON d.idDriver = v.Driver_idDriver";
         $query .= " GROUP BY v.Accident_Info_idAccident_Info LIMIT 10";*/
         
         $query="SELECT * , dw.name as dayname , lc.name as light , wc.name as weather,count(*) from victim v 
         LEFT JOIN driver d ON d.idDriver=v.Driver_idDriver
         LEFT JOIN accident_info ai ON ai.idAccident_info = v.Accident_Info_idAccident_Info
         LEFT JOIN accident_location al ON al.idAccident_location = ai.idAccident_location
         LEFT JOIN hospital h ON h.idhospital=v.hospital_idhospital
         LEFT JOIN day_week dw ON ai.Day_of_Week = dw.idDayWeek
         LEFT JOIN weather_condition wc ON ai.Weather_condition = wc.idWeather
         LEFT JOIN light_condition lc ON ai.Light_condition = lc.idLight
         LEFT JOIN injury_severity ins ON ins.idinjury_severity=v.InjurySeverity
         LEFT JOIN victim_category vc ON vc.idvictim_category = v.VictimCategory";

        /*$query.=" WHERE ai.Accident_date like '%-".$month."-".date('Y')."'
         GROUP BY v.Accident_Info_idAccident_Info ORDER BY v.idVictim desc LIMIT 100";*/


         $query.=" WHERE STR_TO_DATE(ai.Accident_date,'%d-%m-%Y')>=STR_TO_DATE('".$from_date."','%d-%m-%Y') AND STR_TO_DATE(ai.Accident_date,'%d-%m-%Y')<=STR_TO_DATE('".$to_date."','%d-%m-%Y')  
         GROUP BY v.Accident_Info_idAccident_Info ORDER BY v.idVictim desc";

        $accidents = DB::select($query);


        $data = 
        [
            'accidents'=>$accidents,
            'from_date'=>$from_date,
            'to_date'=>$to_date
        ];

        return view('dashboard.accidents')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from_date = $request['from_date'];
        $to_date = $request['to_date'];

         $query="SELECT * , dw.name as dayname , lc.name as light , wc.name as weather,count(*) from victim v 
         LEFT JOIN driver d ON d.idDriver=v.Driver_idDriver
         LEFT JOIN accident_info ai ON ai.idAccident_info = v.Accident_Info_idAccident_Info
         LEFT JOIN accident_location al ON al.idAccident_location = ai.idAccident_location
         LEFT JOIN hospital h ON h.idhospital=v.hospital_idhospital
         LEFT JOIN day_week dw ON ai.Day_of_Week = dw.idDayWeek
         LEFT JOIN weather_condition wc ON ai.Weather_condition = wc.idWeather
         LEFT JOIN light_condition lc ON ai.Light_condition = lc.idLight
         LEFT JOIN injury_severity ins ON ins.idinjury_severity=v.InjurySeverity
         LEFT JOIN victim_category vc ON vc.idvictim_category = v.VictimCategory";

        /*$query.=" WHERE ai.Accident_date like '%-".$month."-".date('Y')."'
         GROUP BY v.Accident_Info_idAccident_Info ORDER BY v.idVictim desc LIMIT 100";*/


         $query.=" WHERE STR_TO_DATE(ai.Accident_date,'%d-%m-%Y')>=STR_TO_DATE('".$from_date."','%d-%m-%Y') AND STR_TO_DATE(ai.Accident_date,'%d-%m-%Y')<=STR_TO_DATE('".$to_date."','%d-%m-%Y')  
         GROUP BY v.Accident_Info_idAccident_Info ORDER BY v.idVictim desc";

        $accidents = DB::select($query);


        $data = 
        [
            'accidents'=>$accidents,
            'from_date'=>$from_date,
            'to_date'=>$to_date
        ];

        return view('dashboard.accidents')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
