<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Phpml\Clustering\KMeans;
//use Phpml\Classification\SVC;
use Phpml\Classification\KNearestNeighbors;
use Phpml\SupportVectorMachine\Kernel;
class StatisticsController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        /*$samples = [['alpha', 'beta', 'epsilon'], ['alpha', 'beta', 'theta'], ['alpha', 'beta', 'epsilon'], ['alpha', 'beta', 'theta']];
        $labels  = [];
        $associator = new Apriori($support = 0.5, $confidence = 0.5);
        $associator->train($samples, $labels);
        print_r($associator->getRules());*/
        
        /*$accidents = DB::select("SELECT * FROM accidents_view");
        $locations = [];
        foreach ($accidents as $key => $accident)
        {
             array_push($locations,[$accident->latitude,$accident->longitude]);
        }

        //$samples = [[1, 1], [8, 7], [1, 2], [7, 8], [2, 1], [8, 9]];
        $kmeans = new KMeans(4);
        $kmeans->cluster($locations);
        $results = $kmeans->cluster($locations);
        foreach ($results as $key=>$result)
        {
            echo $result[0][0].','.$result[0][1].'<br/>';
            
        }
        
        exit();*/

   /////////////////////////////////////////////////////////////////////////////////     

   /*     $samples = [];
        $labels = [];
        
        
        $accidents = DB::select("SELECT * FROM accidents_view av LEFT JOIN patient_status p ON p.idStatus=av.patient_status");

        foreach ($accidents as $key => $accident)
        {
            $month = explode("-",$accident->Accident_date)[1];
            array_push($samples,[$month,$accident->victim_age,$accident->longitude,$accident->latitude]);
            array_push($labels,$key);
        }
        
        
        
        
        $classifier = new KNearestNeighbors();
        
        $classifier->train($samples, $labels);
        
        $predict_key = $classifier->predict([10,28,35.3354,32.222154]);
        
        echo $accidents[$predict_key]->status.'<br/>';

        echo $accidents[$predict_key]->chief_complaint;
        
        exit();
        */







        $query = "SELECT * FROM user u";
        $users = DB::select($query);

        $day_statisitcs_query = "SELECT count(*) as total ,dw.name";
        $day_statisitcs_query.=" FROM accident_info";
        $day_statisitcs_query.=" LEFT JOIN day_week dw ON dw.idDayWeek= accident_info.Day_of_Week";
        $day_statisitcs_query.=" GROUP BY Day_of_Week";

        $day_statistics = DB::select($day_statisitcs_query);
        $overall_total = 0;

        foreach ($day_statistics as $day) 
        {
            $overall_total+=$day->total;
        }

        $light_query = "SELECT count(*) as total , lc.name";
        $light_query .= " FROM accident_info ai";
        $light_query .= " LEFT JOIN light_condition lc ON lc.idLight=ai.Light_condition";
        $light_query .= " GROUP BY ai.Light_condition";

        $light_statistics = DB::select($light_query);

$age_query = "SELECT v.victim_age as age,count(*) as total FROM `victim` v WHERE `victim_age`!='' GROUP BY v.victim_age";
        $age_statistics=DB::select($age_query);

        $btw_0_18 = 0;
        $btw_19_30=0;
        $btw_31_60=0;
        $greater_60=0;

        foreach ($age_statistics as $ages) 
        {
            $age = $ages->age;
            $total = $ages->total;

             if($age>=0 && $age<=18)
             {
                 $btw_0_18+=$total;
             }
             else if($age>=19 && $age<=30)
             {
               $btw_19_30 +=$total;
             }
             else if($age>=31 && $age<=60)
             {
               $btw_31_60+=$total;
             }
             else if($age>60)
             {
               $greater_60+=$total;
             }
        }


    $clustering_query = "SELECT * FROM clustering";
    $clustering_statistics = DB::select($clustering_query);    

        $data=
        [
            'users'=>$users,
            'day_statistics'=>$day_statistics,
            'light_statistics'=>$light_statistics,
            'btw_0_18'=>($btw_0_18*100)/$overall_total,
            'btw_19_30'=>($btw_19_30*100)/$overall_total,
            'btw_31_60'=>($btw_31_60*100)/$overall_total,
            'greater_60'=>100-((($btw_0_18*100)/$overall_total)+(($btw_19_30*100)/$overall_total)+(($btw_31_60*100)/$overall_total)),
            'clustering_statistics'=>$clustering_statistics,
            'total_accidents'=>$overall_total
        ];

        return view('dashboard.statistics')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subject= $request['subject'];
        $body = $request['body'];
        $is_checked = 0;

        if($subject=='' || $body=='')
        {
              $request->session()->put('message', 'Please Fill Subject And Body');
              $request->session()->put('type', 'danger');
              return redirect('/statistics');
              exit();
        }
        else
        {
        $time  = date('H:i',(round(time() / (5 * 60)) * (5 * 60))+10800);
        $dayofweek = date('N')+1;

        if(!isset($_POST['user_all']))
        {

            $query = "SELECT * FROM user u";
            //$query .= " WHERE u.userType=1";
            
            $users = DB::select($query);
         
            $checked = array();

            foreach ($users as $key => $value) 
            {
                $user_id =  $users[$key]->userId;
                if(isset($_POST['user_'.$user_id]))
                {
                    $checked[]=$user_id;
                    $is_checked=1;
                }
            }

            if($is_checked==0)
            {
                $request->session()->put('message', 'No users selected to send notification');
                $request->session()->put('type', 'danger');
                return redirect('/statistics');
                exit();
            }
           else 
           $notification_query = DB::table('user')->whereIn('userId',$checked)->get();
       }
       else
       {
            $query = "SELECT * FROM user u";
            //$query .= " WHERE u.userType=1";
            $notification_query = DB::select($query);
       }

       $tokens = array();

        foreach ($notification_query as $key => $token) 
        {
                $tokens[]= $notification_query[$key]->pnToken;

        
            DB::table('notification')->insert
            (
                [
                 'user_id' => $notification_query[$key]->userId,
                 'subject' => $subject,
                 'body' => $body,
                 'notification_date' => date('d-m-y'),
                 'notification_time'=>$time,
                 'notification_day' => $dayofweek

               ]
            );
                
        }
        
       $fields = array
        (
            'registration_ids'  => $tokens,
            "notification" => array(
                    "body" => $body,
                    "title" => $subject,
                    "sound"=>'default',
                    "click_action"=>"FCM_PLUGIN_ACTIVITY"
            ),
            "data"=>array(              
                "param1"=>"value1",
                "param2"=>"value2"
            )
            
            );

            $headers = array
            (
                    'Authorization: key=' . "AIzaSyDvB4qzCQlppMg_V7-gcePjtbyEpxVBir8",
                    'Content-Type: application/json'
            );

            $url = 'https://fcm.googleapis.com/fcm/send';
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            curl_close( $ch );

            $request->session()->put('message', 'Notification Successfully Sent');
            $request->session()->put('type', 'success');
            return redirect('/statistics');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
