<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {

        $user_id = $request->session()->get('user_id', '0');

        $query = "SELECT * FROM user u ";
        $query .= " LEFT JOIN admin_role ar ON ar.user_id=u.userId";
        $query .= " WHERE u.userId=".$user_id;

        $profile = DB::select($query);

        return view('dashboard.profile')->with('profile',$profile);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->session()->get('user_id', '0');
        $username = $request['username'];
        $email = $request['email'];
        $role = $request['role'];
        $username_changed = $request['username_changed'];
        $old_password = $request['old_password'];
        $new_password = $request['new_password'];
        $confirm_password = $request['confirm_password'];
        $password_changed = 0;

        $query = "SELECT * FROM user u ";
        $query .= " LEFT JOIN admin_role ar ON ar.user_id=u.userId";
        $query .= " WHERE u.userId=".$user_id;

        $profile = DB::select($query);

        if($email=='' || $role=='')
        {
            $request->session()->put('message', 'Please Fill Email And Role fields');
            $request->session()->put('type', 'danger');
            return redirect('/profile');
            exit();
        }
        else
        {
            if($username_changed == 1)
            {    
                if($username=='')
                {
                    $request->session()->put('message', 'Please Fill Username');
                    $request->session()->put('type', 'danger');
                    return redirect('/profile');
                    exit();
                }   
                else
                {  
                    $query = "SELECT * FROM user WHERE userName='".$username."' AND userId!=".$user_id;
                    $check_username = DB::select($query);
                    if(count($check_username) >0)
                    {
                        $request->session()->put('message', 'Username Already Exist');
                        $request->session()->put('type', 'danger');
                        return redirect('/profile');
                        exit();
                    }
                }
            }

            if($old_password!='' && $new_password!='' && $confirm_password!='')
            {
                if($new_password!=$confirm_password)
                {
                    $request->session()->put('message', 'Password doesnot match');
                    $request->session()->put('type', 'danger');
                    return redirect('/profile');
                    exit();
                }
                else
                {
                    $query = "SELECT password FROM user WHERE userId=".$user_id." AND password='".md5($old_password)."'";
                    $check_password = DB::select($query);

                    if(count($check_password)==0)
                    {
                        $request->session()->put('message', 'Invalid Password');
                        $request->session()->put('type', 'danger');
                        return redirect('/profile');
                        exit();
                    }
                    else
                    {
                        $password_changed=1;
                    }
                }

            }
                $params = array();
                $params['Email']=$email;
                if($username_changed==1)
                  $params['userName']=$username;
                if($password_changed==1)
                  $params['password'] = md5($new_password);

               DB::table('user')
                ->where('userId', $user_id)
                ->update(
                    $params
                ); 

                 DB::table('admin_role')
                ->where('user_id', $user_id)
                ->update(
                    ['role'=>$role]
                ); 


                $request->session()->put('admin_email', $email);
                $request->session()->put('admin_username', $username);
                $request->session()->put('admin_role', $role);

                $query = "SELECT * FROM user u ";
                $query .= " LEFT JOIN admin_role ar ON ar.user_id=u.userId";
                $query .= " WHERE u.userId=".$user_id;

                $profile = DB::select($query);

                 $data = 
                [
                    'profile'=>$profile
                ];
                $request->session()->put('message', 'Edited Successfully');
                $request->session()->put('type', 'success');
                return redirect('/accidents');   
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
