<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Phpml\Association\Apriori;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $query = "SELECT *,u.userId as id FROM user u";
        $query .= " LEFT JOIN driver d ON d.userId=u.userId";
        $query .=" LEFT JOIN user_type ut ON u.userType=ut.idUserType";
        $query .= " WHERE u.userType!=5";
        
        $users = DB::select($query);

        $user_type = ["1"=>"success","2"=>"danger","3"=>"primary","4"=>"warning"];

         $data = 
         [
            'users'=>$users,
            'user_type'=>$user_type
         ];

        return view('dashboard.users')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
