<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class EditsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
         $query = "SELECT * , dw.name as day,lc.name as light,wc.name as weather FROM victim v";
         $query .= " LEFT JOIN accident_info ai ON v.Accident_Info_idAccident_Info=ai.idAccident_info";
         $query .= " LEFT JOIN accident_location al ON al.idAccident_location=ai.idAccident_location";
         $query .= " LEFT JOIN day_week dw ON dw.idDayWeek=ai.Day_of_Week";
         $query .= " LEFT JOIN light_condition lc ON lc.idLight=ai.Light_condition";
         $query .= " LEFT JOIN weather_condition wc ON wc.idWeather=ai.Weather_condition";
         $query .= " WHERE v.Accident_Info_idAccident_Info=".$id;
         $accident = DB::select($query);

         $query = "SELECT * FROM day_week";
         $day_week = DB::select($query);

         $query = "SELECT * FROM light_condition";
         $light_condition = DB::select($query);

         $query = "SELECT * FROM weather_condition";
         $weather_condition = DB::select($query);

         $query = "SELECT * FROM day_week";
         $day_week = DB::select($query);

         $query ="SELECT *,vc.type as victim_type,inj.type as injury_type,h.Name as hospital_name FROM victim v";
         $query.= " LEFT JOIN victim_category vc ON v.VictimCategory=vc.idvictim_category";
         $query.= " LEFT JOIN injury_severity inj ON  inj.idinjury_severity=v.InjurySeverity";
         $query.= " LEFT JOIN hospital h ON h.idhospital=v.hospital_idhospital";
         $query.= " LEFT JOIN driver d ON d.idDriver=v.Driver_idDriver";
         $query.= " WHERE Accident_Info_idAccident_Info=".$id;
         $victims = DB::select($query);
     
         $data = 
         [
            'accident'=>$accident,
            'day_week' => $day_week,
            'victims' => $victims,
            'light_condition' => $light_condition,
            'weather_condition' => $weather_condition
         ];

         return view('dashboard.edit')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $accident_id =  $request['accident_id'];
        $accident_date = $request['accident_date'];
        $accident_day = $request['accident_day'];
        $accident_time = $request['accident_time'];
        $light_condition = $request['light_condition'];
        $weather_condition = $request['weather_condition'];
        $speed_limit = $request['speed_limit'];
        $acc_time = mb_substr($accident_time, 0, 2);

        if($accident_date=='' || $accident_day=='' || $light_condition=='' || $weather_condition=='' || $speed_limit=='')
        {
            $request->session()->put('message', 'Please Fill All Fields');
            $request->session()->put('type', 'danger');
            return redirect('/edit/'.$accident_id);
            exit();
        }
        else
        {
           DB::table('accident_info')
            ->where('idAccident_info', $accident_id)
            ->update(
            [
               'Accident_date' => $accident_date,
               'Day_of_Week' => $accident_day,
               'Accident_time' => $accident_time,
               'acc_time' => $acc_time,
               'Light_condition' => $light_condition,
               'Weather_condition' => $weather_condition,
               'Speed_limit' => $speed_limit
            ]); 

            $request->session()->put('message', 'Successfully Edited');
            $request->session()->put('type', 'success');
            return redirect('/accidents');
         }
           //return redirect('/edit/'.$accident_id);
            //return redirect()->route( '{accident_id}.store',$accident_id )->with( [ 'message' => 's' ] );
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
