<?php
require_once 'connection.php';

$response = array();

$accident_info_id = $_POST['accident_id'];
//$accident_info_id = 2;

$query = "SELECT *,vc.type as category_name,inj.type as injury_name,h.Name as hospital_name,ps.status as status_name from victim v";
$query .= " LEFT JOIN driver d ON v.Driver_idDriver=d.idDriver";
$query .= " LEFT JOIN victim_category vc ON vc.idvictim_category=v.VictimCategory";
$query .= " LEFT JOIN injury_severity inj ON inj.idinjury_severity=v.InjurySeverity";
$query .= " LEFT JOIN hospital h ON h.idhospital=v.hospital_idhospital";
$query .= " LEFT JOIN patient_status ps ON ps.idStatus=v.patient_status";
$query .= " WHERE Accident_Info_idAccident_Info=".$accident_info_id;

$result = mysqli_query($con, $query);

if(mysqli_num_rows($result)==0)
{
	$response['success'] = -1;
	echo json_encode($response);
	exit();
}

if (mysqli_num_rows($result) >0) {
	
	$response["victims"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$victim_list = array();
		$victim_list['idVictim'] = $row['idVictim'];
		$victim_list['Driver_idDriver'] = $row['Driver_idDriver'];
		$victim_list['Accident_Info_idAccident_Info'] = $row['Accident_Info_idAccident_Info'];
		$victim_list['victim_name'] = $row['victim_name'];
		$victim_list['First_name'] = $row['First_name'];
		$victim_list['Middle_name'] = $row['Middle_name'];
		$victim_list['Last_name'] = $row['Last_name'];
		$victim_list['VictimCategory'] = $row['VictimCategory'];
		$victim_list['InjurySeverity'] = $row['InjurySeverity'];
		$victim_list['hospital_idhospital'] = $row['hospital_idhospital'];
		$victim_list['additionalinfo'] = $row['additionalinfo'];
		$victim_list['category_name'] = $row['category_name'];
		$victim_list['injury_name'] = $row['injury_name'];
		$victim_list['hospital_name'] = $row['hospital_name'];
		$victim_list['status_name'] = $row['status_name'];
		$victim_list['patient_status'] = $row['patient_status'];
		$victim_list['chief_complaint'] = $row['chief_complaint'];
		$victim_list['victim_age'] = $row['victim_age'];
		$victim_list['victim_cause'] = $row['victim_cause'];
		array_push($response["victims"], $victim_list);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}
echo json_encode($response);

?>
  
  
  
 