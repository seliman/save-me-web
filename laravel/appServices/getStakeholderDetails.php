<?php
require_once 'connection.php';

$user_id = $_POST['user_id'];

$query = "SELECT * FROM user  WHERE userId=".$user_id;


$result = mysqli_query($con, $query);
$response = array();

if (mysqli_num_rows($result) >0) 
{
	
	$response["stakeholders_info"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$stakeholders_info = array();
		$stakeholders_info['userName'] = $row['userName'];
		$stakeholders_info['Email'] = $row['Email'];
		array_push($response["stakeholders_info"], $stakeholders_info);
	}
	
	$response["stakeholders_info_success"] = 1;
}
else
{
	$response["stakeholders_info_success"] = 0;
}

////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM hospital ";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["hospital"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$hospital = array();
		$hospital['id'] = $row['idhospital'];
		$hospital['value'] = $row['Name'];
		array_push($response["hospital"], $hospital);
	}
	
	$response["hospital_success"] = 1;
}
else
{
	$response["hospital_success"] = 0;
}

////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM injury_severity ";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["injury_severity"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$injury_severity = array();
		$injury_severity['id'] = $row['idinjury_severity'];
		$injury_severity['value'] = $row['type'];
		array_push($response["injury_severity"], $injury_severity);
	}
	
	$response["injury_severity_success"] = 1;
}
else
{
	$response["injury_severity_success"] = 0;
}



////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM victim_category ";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["victim_category"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$victim_category = array();
		$victim_category['id'] = $row['idvictim_category'];
		$victim_category['value'] = $row['type'];
		array_push($response["victim_category"], $victim_category);
	}
	
	$response["victim_category_success"] = 1;
}
else
{
	$response["victim_category_success"] = 0;
}

////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM user u";
$query .= " LEFT JOIN driver d ON d.userId = u.userId";
$query .= " WHERE u.userType=1";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["user"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$driver = array();
		$driver['id'] = $row['idDriver'];
		$driver['value'] = $row['userName'];
		array_push($response["user"], $driver);
	}
	
	$response["user_success"] = 1;
}
else
{
	$response["user_success"] = 0;
}

////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM vehicle v";
$query .= " LEFT JOIN driver d ON v.driver_id=d.idDriver";
$query .= " LEFT JOIN user u ON u.userId=d.userId";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["vehicles"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$driver = array();
		$driver['id'] = $row['idVehicle'];
		$driver['value'] = $row['Vehicle_model'].' '.$row['vehicle_make'].' - '.$row['userName'];
		array_push($response["vehicles"], $driver);
	}
	
	$response["vehicles_success"] = 1;
}
else
{
	$response["vehicles_success"] = 0;
}

////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM policy";

$result = mysqli_query($con, $query);

if (mysqli_num_rows($result) >0) {
	
	$response["policy_dropdown"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$policy = array();
		$policy['id'] = $row['idPolicy'];
		$policy['value'] = $row['PolicyNumber'];
		array_push($response["policy_dropdown"], $policy);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}

////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM patient_status";

$result = mysqli_query($con, $query);

if (mysqli_num_rows($result) >0) {
	
	$response["patient_status"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$patient_status = array();
		$patient_status['id'] = $row['idStatus'];
		$patient_status['value'] = $row['status'];
		array_push($response["patient_status"], $patient_status);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}


echo json_encode($response);
?>