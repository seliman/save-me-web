<?php
/*$dt = DateTime::createFromFormat('y', date('y'));
$year = $dt->format('Y');

$dt = DateTime::createFromFormat('m', date('m'));
$month = $dt->format('m');
$month = (substr($month,0,1) == "0")?substr($month,1,1):$month;
//echo substr($month, 1,1); ;

$dt = DateTime::createFromFormat('d', date('d'));
$day = $dt->format('d');
$day = (substr($day,0,1) == "0")?substr($day,1,1):$day;

echo $day."-".$month."-".$year;*/




/*
SELECT * , dw.name as day , lc.name as light , wc.name as weather from victim v 
LEFT JOIN driver d ON d.idDriver=v.Driver_idDriver
LEFT JOIN accident_info ai ON ai.idAccident_info = v.Accident_Info_idAccident_Info
LEFT JOIN accident_location al ON al.idAccident_location = ai.idAccident_location
LEFT JOIN hospital h ON h.idhospital=v.hospital_idhospital
LEFT JOIN day_week dw ON ai.Day_of_Week = dw.idDayWeek
LEFT JOIN weather_condition wc ON ai.Weather_condition = wc.idWeather
LEFT JOIN light_condition lc ON ai.Light_condition = lc.idLight
LEFT JOIN injury_severity ins ON ins.idinjury_severity=v.InjurySeverity
LEFT JOIN victim_category vc ON vc.idvictim_category = v.VictimCategory

WHERE ai.Accident_date LIKE '%-7-2018'
*/

require_once 'connection.php';
$day = $_POST['day'];
$month = $_POST['month'];
$year = $_POST['year'];

$query = "SELECT * , dw.name as day , lc.name as light , wc.name as weather,count(*) from victim v 
LEFT JOIN driver d ON d.idDriver=v.Driver_idDriver
LEFT JOIN accident_info ai ON ai.idAccident_info = v.Accident_Info_idAccident_Info
LEFT JOIN accident_location al ON al.idAccident_location = ai.idAccident_location
LEFT JOIN hospital h ON h.idhospital=v.hospital_idhospital
LEFT JOIN day_week dw ON ai.Day_of_Week = dw.idDayWeek
LEFT JOIN weather_condition wc ON ai.Weather_condition = wc.idWeather
LEFT JOIN light_condition lc ON ai.Light_condition = lc.idLight
LEFT JOIN injury_severity ins ON ins.idinjury_severity=v.InjurySeverity
LEFT JOIN victim_category vc ON vc.idvictim_category = v.VictimCategory";
if($day == '')
 	$query .= " WHERE ai.Accident_date LIKE '%-".$month."-".$year."'";

else
	$query .= " WHERE ai.Accident_date='".$day."-".$month."-".$year."'";

$query .="GROUP BY v.Accident_Info_idAccident_Info  ORDER BY v.idVictim desc"; 

$result = mysqli_query($con,$query);

if(mysqli_num_rows($result) ==0)
{
	$response['success'] = -1;
}
else if (mysqli_num_rows($result) >0) 
{
	
	$response["accidents_calender"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$accidents = array();
		$accidents['idVictim'] = $row['idVictim'];
		$accidents['Accident_Info_idAccident_Info'] = $row['Accident_Info_idAccident_Info'];
		$accidents['idAccident_info'] = $row['idAccident_info'];
		$accidents['idAccident_location'] = $row['idAccident_location'];
		$accidents['Accident_date'] = $row['Accident_date'];
		$accidents['Day_of_Week'] = $row['Day_of_Week'];
		$accidents['Accident_time'] = $row['Accident_time'];
		$accidents['city'] = $row['city'];
		$accidents['country'] = ($row['country']=='')?'Not Specified':$row['country'];
		$accidents['street'] = ($row['street']=='')?'Not Specified':$row['street'];
		$accidents['Light_condition'] = ($row['Light_condition']=='')?'Not Specified':$row['Light_condition'];
		$accidents['Weather_condition'] = ($row['Weather_condition']=='')?'Not Specified':$row['Weather_condition'];
		$accidents['Speed_limit'] = ($row['Speed_limit']=='')?'Not Specified':$row['Speed_limit'];
		$accidents['day'] = $row['day'];
		$accidents['weather'] = ($row['weather']=='')?'Not Specified':$row['weather'];
		$accidents['light'] = ($row['light']=='')?'Not Specified':$row['light'];
		array_push($response["accidents_calender"], $accidents);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}

echo json_encode($response);
?>