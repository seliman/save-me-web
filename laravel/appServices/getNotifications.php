<?php
require_once 'connection.php';

$response = array();

$stakeholder_id = $_POST['stakeholder_id'];
$user_id = $_POST['user_id'];


$query = "SELECT * FROM notification n";
$query .= " LEFT JOIN day_week dw ON dw.idDayWeek=n.notification_day";
$query .= " LEFT JOIN accident_location al ON al.idAccident_location=n.idaccident_location";

if($stakeholder_id!=null)
	$query .= " WHERE stakeholder_id=".$stakeholder_id." OR user_id=".$user_id;
else
	$query .= " WHERE user_id=".$user_id;

$query .= " ORDER BY n.idNotification DESC  LIMIT 100";	

$response['query']=$query;
$result = mysqli_query($con, $query);

if(mysqli_num_rows($result)==0)
{
	$response['success'] = -1;
	echo json_encode($response);
	exit();
}

if (mysqli_num_rows($result) >0) {
	
	$response["notifications"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$notifications = array();
		$notifications['idNotification'] = $row['idNotification'];
		$notifications['receipt_id'] = $row['receipt_id'];
		$notifications['user_id'] = $row['user_id'];
		$notifications['subject'] = $row['subject'];
		$notifications['body'] = $row['body'];
		$notifications['notification_date'] = $row['notification_date'];
		$notifications['name'] = $row['name'];
		$notifications['notification_time'] = $row['notification_time'];
		$notifications['longitude'] = $row['longitude'];
		$notifications['latitude'] = $row['latitude'];
		$notifications['city'] = $row['city'];
		$notifications['country'] = $row['country'];
		array_push($response["notifications"], $notifications);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}
echo json_encode($response);

?>
  
  
  
 