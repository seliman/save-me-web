<?php
require_once 'connection.php';
include 'sendNotification.php';

/************************************* K Nearest Neighbors *************************************/
require_once 'php-ai/php-ml/src/Helper/Trainable.php';
require_once 'php-ai/php-ml/src/Estimator.php';
require_once 'php-ai/php-ml/src/Classification/Classifier.php';
require_once 'php-ai/php-ml/src/Helper/Predictable.php';
require_once 'php-ai/php-ml/src/Math/Distance.php';
require_once 'php-ai/php-ml/src/Math/Distance/Euclidean.php';
use Phpml\Classification\KNearestNeighbors;
require_once 'php-ai/php-ml/src/Classification/KNearestNeighbors.php';

$accident_lng = $_POST['lng'];
$accident_lat = $_POST['lat'];
$accident_city = $_POST['city'];
$accident_country = $_POST['country'];
$accident_street = $_POST['street'];
$accident_weather = $_POST['weather'];
$driver_id = $_POST['driver_id'];
$saved_by = $_POST['saved_by'];
$stakeholder = $_POST['stakeholders'];

$start_lat = $_POST['start_lat'];
$start_lng = $_POST['start_lng'];
$start_time = $_POST['start_time'];
$info_type = $_POST['info_type'];

$notify_subject = '';
$notify_body = '';
$samples = [];
$labels = [];

$dt = DateTime::createFromFormat('y', date('y'));
$year = $dt->format('Y');

$dt = DateTime::createFromFormat('m', date('m'));
$month = $dt->format('m');
$month = (substr($month,0,1) == "0")?substr($month,1,1):$month;
//echo substr($month, 1,1); ;

$dt = DateTime::createFromFormat('d', date('d'));
$day = $dt->format('d');
$day = (substr($day,0,1) == "0")?substr($day,1,1):$day;

$accident_date = $day."-".$month."-".$year;
$dayofweek = date('N')+1;
$dt = new DateTime();
$accident_time =  ($dt->format('H')+3);

$accident_time =  date('H:i',(round(time() / (5 * 60)) * (5 * 60))+10800);
$acc_time =  date('H',(round(time() / (5 * 60)) * (5 * 60))+10800);

$light_condition = '';

if($acc_time >=18 && $acc_time<24)
	$light_condition = 2;
else
	$light_condition = 1;

$weather_id = '';

$response = array();

$query = "SELECT * FROM weather_condition WHERE name='".$accident_weather."'";

$result = mysqli_query($con,$query);

if(mysqli_num_rows($result) >0)
{
	while ($row = mysqli_fetch_array($result))
	{
		$weather_id =  $row['idWeather'];
	}
}
else
{
	$insert_weather = mysqli_query($con,"INSERT INTO weather_condition SET name='".$accident_weather."'");
	$weather_id = mysqli_insert_id($con);
}

$insert_accident_location = "INSERT INTO accident_location SET city='".$accident_city."',
country='".$accident_country."',street='".$accident_street."',longitude='".$accident_lng."'
,latitude='".$accident_lat."'";

$insert_location_result = mysqli_query($con,$insert_accident_location);
$accident_location_id = mysqli_insert_id($con);

$insert_accident_info = "INSERT INTO accident_info SET idAccident_location=".$accident_location_id."
,Accident_date='".$accident_date."',Day_of_Week='".$dayofweek."',Accident_time='".$accident_time."',acc_time='".$acc_time."',Weather_condition='".$weather_id."',Light_condition='".$light_condition."'";

if($info_type==1)
{
	$speed_limit='';

	$startTime = new DateTime($start_time);
	$endTime = new DateTime(date("H:i:s"));
	$duration = $startTime->diff($endTime);
	$hours = $duration->format("%H").'<br/>';
	$minutes = $duration->format("%I").'<br/>';
	$seconds = (($hours*3600)+($minutes*60)+$duration->format("%S"));

	$point1 = array("lat" => $start_lat, "long" => $start_lng);
	$point2 = array("lat" => $accident_lat, "long" => $accident_lng);

	$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']);

	$speed_limit = number_format(($km*1000)/$seconds,3);

	$insert_accident_info.=" ,Speed_limit='".$speed_limit."'";
}

$insert_info_result = mysqli_query($con,$insert_accident_info);
$accident_info_id = mysqli_insert_id($con);

if($driver_id !='')
{
	$insert_victim = "INSERT INTO victim SET Driver_idDriver=".$driver_id."
	,Accident_Info_idAccident_Info=".$accident_info_id;

	if($_POST['driver_age']!='' && $_POST['driver_age']!='null' && $_POST['driver_age']!=null)
	{
		$insert_victim.=",victim_age='".ageCalculator($_POST['driver_age'])."'";
	}

	////////////////////////////Data Mining part///////////////////////

	  $accidents_query = "SELECT * FROM accidents_view";
	  $accidents =mysqli_query($con,$accidents_query);
  	  $key=0;
  	  $data_mining_age='';

  	  if($_POST['driver_age']!='' && $_POST['driver_age']!='null' && $_POST['driver_age']!=null)
	  {
			$data_mining_age= ageCalculator($_POST['driver_age']);
	  }

	  while ($row = mysqli_fetch_array($accidents))
	  {
		$acc_month = explode("-",$row['Accident_date'])[1];
		array_push($samples,[$acc_month,$row['victim_age'],$row['longitude'],$row['latitude']]);
		array_push($labels,$row['idVictim']);
		$key++;
	  }

		$classifier = new KNearestNeighbors();
    	
    	$classifier->train($samples, $labels);
    	
    	$predict_key = $classifier->predict([$month,$data_mining_age,$accident_lat,$accident_lng]);

    	$get_statistics = mysqli_query($con,"SELECT * FROM accidents_view av LEFT JOIN patient_status p ON p.idStatus=av.patient_status WHERE av.idVictim=".$predict_key);
    	$statistics_result = mysqli_fetch_array($get_statistics);

    	$notify_subject = 'An accident occured at <b>' . $accident_city .', ' . $accident_country.'</b>';
    	$notify_body = 'The Application prediction concerning the victim<br/><b>Chief Complaint:</b> '.$statistics_result['chief_complaint']."<br/> <b>Status:</b> ".$statistics_result['status']; 
    	// echo $statistics_result['chief_complaint'].'<br/>';
    	// echo $statistics_result['status'].'<br/>';
    ////////////////////////////////////////////////////////////////////

}
else
{
	$insert_victim = "INSERT INTO victim SET Accident_Info_idAccident_Info=".$accident_info_id
	.',saved_by='.$saved_by;

	$notify_subject = 'An accident occured at <b>' . $accident_city .', ' . $accident_country.'</b>';
	$notify_body = 'The Application cannot predict victim situation';
}

$insert_victim_result = mysqli_query($con,$insert_victim); 

$tokens = array();

$send_notification_query = "SELECT *
FROM user uv
WHERE uv.userType IN (SELECT u.idUserType FROM stakeholder s 
LEFT JOIN user_type u ON u.type = s.stakeholder_name
WHERE s.idStakeholder IN(".$stakeholder."))"; 


$send_result = mysqli_query($con,$send_notification_query);

if(mysqli_num_rows($send_result) >0)
{
	while ($row = mysqli_fetch_array($send_result))
	{
		$tokens[] = $row['pnToken'];
	}
}

if($send_result)
{
	sendNotification($tokens,$notify_subject,$notify_body);

	$get_stakeholders_expolde = explode(',', $stakeholder);

	if($info_type==1)
		$receipt_id=$driver_id;
	else
		$receipt_id = $saved_by;
	
	foreach ($get_stakeholders_expolde as $key => $value) 
	{
		$query = " INSERT INTO notification SET receipt_id=".$receipt_id.",stakeholder_id=".$value;
		$query .= " , subject='".$notify_subject."',body='".$notify_body."',notification_date='".date('d-m-y')."'";
		$query .= ",notification_time='".$accident_time."',notification_day='".$dayofweek."',idaccident_location=".$accident_location_id;	
		
		mysqli_query($con,$query);
	}

}

if(!$insert_location_result || !$insert_info_result || !$insert_victim_result)
{

	$response['success'] = 0;
}
else
{
	$response['success'] = 1;
} 


echo json_encode($response);



function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
	// Calculate the distance in degrees
	$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
	
	// Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
	switch($unit) {
		case 'km':
			$distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
			break;
		case 'mi':
			$distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
			break;
		case 'nmi':
			$distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
	}
	return round($distance, $decimals);
}


function ageCalculator($dob)
{
	if(!empty($dob)){
		$birthdate = new DateTime($dob);
		$today   = new DateTime('today');
		$age = $birthdate->diff($today)->y;
		return $age;
	}else{
		return 0;
	}
}
?>  