<?php
require_once 'connection.php';

$response = array();


$query = "SELECT * FROM policy";

$result = mysqli_query($con, $query);

if (mysqli_num_rows($result) >0) {
	
	$response["policy"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$policy = array();
		$policy['idPolicy'] = $row['idPolicy'];
		$policy['PolicyNumber'] = $row['PolicyNumber'];
		$policy['PolicyEffectiveDate'] = $row['PolicyEffectiveDate'];
		$policy['PolicyExpiryDate'] = $row['PolicyExpiryDate'];
		$policy['PaymentOption'] = $row['PaymentOption'];
		$policy['TotalAmount'] = $row['TotalAmount'];
		$policy['additionalinfo'] = $row['additionalinfo'];
		$policy['createdDate'] = $row['createdDate'];
		array_push($response["policy"], $policy);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}
echo json_encode($response);

?>