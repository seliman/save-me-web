<?php
require_once 'connection.php';

$response = array();


$query = "SELECT * from user_type WHERE idUserType!=5;";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["user_type"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type["id"] = $row['idUserType'];
		$user_type["value"] = $row['type'];
		array_push($response["user_type"], $user_type);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}


//////////////////////////////////////////////////////////////////////

$query = "SELECT * from nationality ";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["nationality"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idNationality'];
		$user_type['value'] = $row['name'];
		array_push($response["nationality"], $user_type);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}

//////////////////////////////////////////////////


$query = "SELECT * from gender ";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["gender"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idGender'];
		$user_type['value'] = $row['type'];
		array_push($response["gender"], $user_type);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}

///////////////////////////////////////////////////

$query = "SELECT * from martial_status ";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["martial"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idMartial'];
		$user_type['value'] = $row['status'];
		array_push($response["martial"], $user_type);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * from license_state ";


$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["license"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idLicenseState'];
		$user_type['value'] = $row['state'];
		array_push($response["license"], $user_type);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}


echo json_encode($response);
?>
  
  
  
 