<?php
require_once 'connection.php';

$response = array();



$driver_id = $_POST['driver_id'];

$query = "SELECT * , dw.name as day , lc.name as light , wc.name as weather from victim v ";
$query .= "LEFT JOIN driver d ON d.idDriver=v.Driver_idDriver ";
$query .=  "LEFT JOIN accident_info ai ON ai.idAccident_info = v.Accident_Info_idAccident_Info ";
$query .= "LEFT JOIN accident_location al ON al.idAccident_location = ai.idAccident_location ";
$query .= "LEFT JOIN hospital h ON h.idhospital=v.hospital_idhospital ";

$query .= "LEFT JOIN day_week dw ON ai.Day_of_Week = dw.idDayWeek ";
$query .= "LEFT JOIN weather_condition wc ON ai.Weather_condition = wc.idWeather ";
$query .= "LEFT JOIN light_condition lc ON ai.Light_condition = lc.idLight ";


$query .= "LEFT JOIN injury_severity ins ON ins.idinjury_severity=v.InjurySeverity ";
$query .= "LEFT JOIN victim_category vc ON vc.idvictim_category = v.VictimCategory WHERE v.Driver_idDriver=".$driver_id;
$query.=" ORDER BY v.idVictim DESC";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) {
	
	$response["accidents"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$accidents = array();
		$accidents['Accident_date'] = $row['Accident_date'];
		$accidents['Day_of_Week'] = $row['Day_of_Week'];
		$accidents['Accident_time'] = $row['Accident_time'];
		$accidents['city'] = $row['city'];
		$accidents['country'] = ($row['country']=='')?'Not Specified':$row['country'];
		$accidents['street'] = ($row['street']=='')?'Not Specified':$row['street'];
		$accidents['Light_condition'] = ($row['Light_condition']=='')?'Not Specified':$row['Light_condition'];
		$accidents['Weather_condition'] = ($row['Weather_condition']=='')?'Not Specified':$row['Weather_condition'];
		$accidents['Speed_limit'] = ($row['Speed_limit']=='')?'Not Specified':$row['Speed_limit'];
		$accidents['day'] = $row['day'];
		$accidents['weather'] = ($row['weather']=='')?'Not Specified':$row['weather'];
		$accidents['light'] = ($row['light']=='')?'Not Specified':$row['light'];
		array_push($response["accidents"], $accidents);
	}
	
	$response["success"] = 1;
}
else if(mysqli_num_rows($result) == 0)
{
	$response["success"] = -1;
}
else
{
	$response["success"] = 0;
}
echo json_encode($response);
?>
  
  
  
 