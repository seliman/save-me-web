<?php
function sendNotification($tokens,$title,$body)
{
	$fields = array
	(
			'registration_ids' 	=> $tokens,
			"notification" => array(
					"body" => $body,
					"title" => $title,
					"sound"=>'default',
					"click_action"=>"FCM_PLUGIN_ACTIVITY"
			),
			"data"=>array(				
				"param1"=>"value1",
				"param2"=>"value2"
			)
			
	);

	$headers = array
	(
			'Authorization: key=' . "AIzaSyDvB4qzCQlppMg_V7-gcePjtbyEpxVBir8",
			'Content-Type: application/json'
	);

	$url = 'https://fcm.googleapis.com/fcm/send';
	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, $url);
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec($ch );
	curl_close( $ch );
	//echo $result;
}
?>

