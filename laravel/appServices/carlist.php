<?php
require_once 'connection.php';

$response = array();



$driver_id = $_POST['driver_id'];

$query = "select * from vehicle v ";
$query .= "LEFT JOIN vehicle_type vt ON v.vehicle_type = vt.idVehicleType ";
$query .= "LEFT JOIN plate_code pc ON v.Vehicle_platecode = pc.idPlateCode WHERE v.driver_id=".$driver_id;

$result = mysqli_query($con, $query);

if (mysqli_num_rows($result) >0) {
	
	$response["carlist"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$carlist = array();
		$carlist['idVehicle'] = $row['idVehicle'];
		$carlist['vehicle_year'] = $row['vehicle_year'];
		$carlist['vehicle_type'] = $row['vehicle_type'];
		$carlist['vehicle_make'] = $row['vehicle_make'];
		$carlist['Vehicle_model'] = $row['Vehicle_model'];
		$carlist['Vehicle_NumberPlate'] = $row['Vehicle_NumberPlate'];
		$carlist['Vehicle_platecode'] = $row['Vehicle_platecode'];
		$carlist['code'] = $row['code'];
		$carlist['type'] = $row['type'];
		array_push($response["carlist"], $carlist);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}
echo json_encode($response);

?>
  
  
  
 