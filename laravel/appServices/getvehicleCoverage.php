<?php
require_once 'connection.php';

$response = array();



$vehicle_id = $_POST['vehicle_id'];

$query = "SELECT *,vc.Active as coverage_active from vehicle_coverage vc ";
$query .= " LEFT JOIN policy p ON p.idPolicy = vc.policy_id";
$query .= " WHERE vc.Vehicle_id=".$vehicle_id;

$result = mysqli_query($con, $query);

if(mysqli_num_rows($result) ==0)
{
	$response['success']=-1;
	echo json_encode($response);
	exit();
}

if (mysqli_num_rows($result) >0) {
	
	$response["vehicle_coverage"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$carlist = array();
		$carlist['idVehicle_coverage'] = $row['idVehicle_coverage'];
		$carlist['Vehicle_Id'] = $row['Vehicle_Id'];
		$carlist['policy_id'] = $row['policy_id'];
		$carlist['coverage_active'] = $row['coverage_active'];
		$carlist['createdDate'] = $row['createdDate'];
		$carlist['PolicyNumber'] = $row['PolicyNumber'];
		array_push($response["vehicle_coverage"], $carlist);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}
$response['query']=$query;
echo json_encode($response);

?>
  
  
  
 