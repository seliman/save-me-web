<?php

require_once 'connection.php';

/************************************* CLUSTERING *************************************/
use Phpml\Clustering\KMeans;
require_once 'php-ai/php-ml/src/Clustering/KMeans/Point.php';
require_once 'php-ai/php-ml/src/Clustering/KMeans/Cluster.php';
require_once 'php-ai/php-ml/src/Clustering/KMeans/Space.php';
require_once 'php-ai/php-ml/src/Clustering/Clusterer.php';
require_once 'php-ai/php-ml/src/Clustering/KMeans.php';

$response = array();



$driver_id = $_POST['driver_id'];
$query = "select * from vehicle v ";
$query .= "LEFT JOIN vehicle_type vt ON v.vehicle_type = vt.idVehicleType ";
$query .= "LEFT JOIN vehicle_year vy ON v.vehicle_year = vy.idVehicleYear ";
$query .= "LEFT JOIN plate_code pc ON v.Vehicle_platecode = pc.idPlateCode WHERE v.driver_id=".$driver_id;

header('Content-Type: application/json; charset=UTF-8');
mysqli_set_charset($con,"utf8");
$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) {
	
	$response["carlist"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$carlist = array();
		$carlist['idVehicle'] = $row['idVehicle'];
		$carlist['vehicle_year'] = $row['vehicle_year'];
		$carlist['vehicle_type'] = $row['vehicle_type'];
		$carlist['vehicle_make'] = $row['vehicle_make'];
		$carlist['Vehicle_model'] = $row['Vehicle_model'];
		$carlist['Vehicle_NumberPlate'] = $row['Vehicle_NumberPlate'];
		$carlist['Vehicle_platecode'] = $row['Vehicle_platecode'];
		$carlist['code'] = $row['code'];
		$carlist['type'] = $row['type'];
		$carlist['name'] = $row['name'];
		array_push($response["carlist"], $carlist);
	}
	
	$response["car_list_success"] = 1;
}
else if (mysqli_num_rows($result) == 0)
{
	$response["car_list_success"] = -1;
}
else
{
	$response["car_list_success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * FROM vehicle_type";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["vehicle_type"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idVehicleType'];
		$user_type['value'] = $row['type'];
		array_push($response["vehicle_type"], $user_type);
	}
	
	$response["vehicle_type_success"] = 1;
}
else
{
	$response["vehicle_type_success"] = 0;
}

/////////////////////////////////////////////////////
$query = "SELECT * FROM plate_code";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["plate_code"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idPlateCode'];
		$user_type['value'] = $row['code'];
		array_push($response["plate_code"], $user_type);
	}
	
	$response["plate_code_success"] = 1;
}
else
{
	$response["plate_code_success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * FROM driver d";
$query .= " LEFT JOIN user u ON d.userId = u.userId";
$query .= " WHERE d.idDriver=".$driver_id;

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["driver_info"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$driver_info = array();
		$driver_info['idDriver'] = $row['idDriver'];
		$driver_info['userName'] = $row['userName'];
		$driver_info['driver_img'] = $row['driver_img'];
		$driver_info['PhoneNumber'] = $row['PhoneNumber'];
		$driver_info['Email'] = $row['Email'];
		$driver_info['use_hardware'] = $row['use_hardware'];
		$driver_info['First_name'] = $row['First_name'];
		$driver_info['Middle_name'] = $row['Middle_name'];
		$driver_info['Last_name'] = $row['Last_name'];
		$driver_info['Dob'] = $row['Dob'];
		$driver_info['Gender'] = $row['Gender'];
		$driver_info['MartialStatus'] = $row['MartialStatus'];
		$driver_info['LicenseIssuedDate'] = $row['LicenseIssuedDate'];
		$driver_info['LicenseIssuedState'] = $row['LicenseIssuedState'];
		$driver_info['LicenseNumber'] = $row['LicenseNumber'];
		$driver_info['driver_address'] = $row['driver_address'];
		$driver_info['SSN'] = $row['SSN'];
		$driver_info['nationality'] = $row['nationality'];
		$driver_info['active'] = $row['active'];
		array_push($response["driver_info"], $driver_info);
	}
	
	$response["driver_info_success"] = 1;
}
else
{
	$response["driver_info_success"] = 0;
}

/////////////////////////////////////////////////////
$query = "SELECT * FROM vehicle_year";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["vehicle_year"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idVehicleYear'];
		$user_type['value'] = $row['name'];
		array_push($response["vehicle_year"], $user_type);
	}
	
	$response["vehicle_year_success"] = 1;
}
else
{
	$response["vehicle_year_success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * FROM nationality";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["nationality"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idNationality'];
		$user_type['value'] = $row['name'];
		array_push($response["nationality"], $user_type);
	}
	
	$response["nationality_success"] = 1;
}
else
{
	$response["nationality_success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * FROM martial_status";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["martial_status"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idMartial'];
		$user_type['value'] = $row['status'];
		array_push($response["martial_status"], $user_type);
	}
	
	$response["martial_status_success"] = 1;
}
else
{
	$response["martial_status_success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * FROM gender";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["gender"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idGender'];
		$user_type['value'] = $row['type'];
		array_push($response["gender"], $user_type);
	}
	
	$response["gender_success"] = 1;
}
else
{
	$response["gender_success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * FROM license_state";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["license_state"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idLicenseState'];
		$user_type['value'] = $row['state'];
		array_push($response["license_state"], $user_type);
	}
	
	$response["license_state_success"] = 1;
}
else
{
	$response["license_state_success"] = 0;
}

/////////////////////////////////////////////////////

$query = "SELECT * FROM stakeholder";

$result = mysqli_query($con, $query);


if (mysqli_num_rows($result) >0) 
{
	
	$response["stakeholder"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$user_type = array();
		$user_type['id'] = $row['idStakeholder'];
		$user_type['value'] = $row['stakeholder_name'];
		$user_type['stakeholder_pnumber'] = $row['stakeholder_pnumber'];
		array_push($response["stakeholder"], $user_type);
	}
	
	$response["stakeholder_success"] = 1;
}
else
{
	$response["stakeholder_success"] = 0;
}

////////////////////////////////////////////////////////////////////////////

$query = "SELECT * FROM vehicle_stakeholder vs";
$query .= " LEFT JOIN vehicle v ON v.idVehicle = vs.vehicle_idvehicle";
$query .= " WHERE v.driver_id=".$driver_id;

$result = mysqli_query($con, $query);

if(mysqli_num_rows($result) == -1)
{
	$response['vehicle_stakeholder_success'] = -1;
	echo json_encode($response);
	exit();
}

else if (mysqli_num_rows($result) >0) 
{
	
	$response["vehicle_stakeholder"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$vehicle_stakeholders = array();
		$vehicle_stakeholders['vehicle_idvehicle'] = $row['vehicle_idvehicle'];
		$vehicle_stakeholders['stakeholder_idstakeholder'] = $row['stakeholder_idstakeholder'];
		array_push($response["vehicle_stakeholder"], $vehicle_stakeholders);
	}
	
	$response["vehicle_stakeholder_success"] = 1;
}
else
{
	$response["vehicle_stakeholder_success"] = 0;
}

////////////////////////////////////////////////////////////////////////////

$accidents_query = "SELECT * FROM clustering";
$accidents =mysqli_query($con,$accidents_query);
$key=0;
$locations=[];
$response["cluserting_lats"] = array();
$response["cluserting_lngs"] = array();
while ($row = mysqli_fetch_array($accidents))
{
	//array_push($locations,[$row['latitude'],$row['longitude']]);
	array_push($response["cluserting_lats"], $row['clustering_latitude']);
	array_push($response["cluserting_lngs"], $row['clustering_longitude']);
}

/*$kmeans = new KMeans(4);
$kmeans->cluster($locations);
$results = $kmeans->cluster($locations);
$response["cluserting_lats"] = array();
$response["cluserting_lngs"] = array();

foreach ($results as $key=>$result)
{
	array_push($response["cluserting_lats"], $result[0][0]);
	array_push($response["cluserting_lngs"], $result[0][1]);
	//echo $result[0][0].','.$result[0][1].'<br/>';
	
}*/

echo json_encode($response);

?>
  
  
  
 