<?php
require_once 'connection.php';

$response = array();



$driver_id = $_POST['driver_id'];

$query = "SELECT *,pc.active as coverage_active from policy_coverage pc ";
$query .= " LEFT JOIN policy p ON p.idPolicy = pc.policy_id";
$query .= " WHERE pc.driver_id=".$driver_id;

$result = mysqli_query($con, $query);

if(mysqli_num_rows($result) ==0)
{
	$response['success']=-1;
	echo json_encode($response);
	exit();
}

if (mysqli_num_rows($result) >0) {
	
	$response["driver_coverage"] = array();
	while ($row = mysqli_fetch_array($result))
	{
		$carlist = array();
		$carlist['idPolicy_coverage'] = $row['idPolicy_coverage'];
		$carlist['driver_id'] = $row['driver_id'];
		$carlist['policy_id'] = $row['policy_id'];
		$carlist['coverage_active'] = $row['coverage_active'];
		$carlist['createdDate'] = $row['createdDate'];
		$carlist['PolicyNumber'] = $row['PolicyNumber'];
		array_push($response["driver_coverage"], $carlist);
	}
	
	$response["success"] = 1;
}
else
{
	$response["success"] = 0;
}
$response['query']=$query;
echo json_encode($response);

?>
  
  
  
 