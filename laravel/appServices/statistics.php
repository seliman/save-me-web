<?php 
require_once 'connection.php';

/************************************* K Nearest Neighbors *************************************/
require_once 'php-ai/php-ml/src/Helper/Trainable.php';
require_once 'php-ai/php-ml/src/Estimator.php';
require_once 'php-ai/php-ml/src/Classification/Classifier.php';
require_once 'php-ai/php-ml/src/Helper/Predictable.php';
require_once 'php-ai/php-ml/src/Math/Distance.php';
require_once 'php-ai/php-ml/src/Math/Distance/Euclidean.php';
use Phpml\Classification\KNearestNeighbors;
require_once 'php-ai/php-ml/src/Classification/KNearestNeighbors.php';

/************************************* CLUSTERING *************************************/
use Phpml\Clustering\KMeans;
require_once 'php-ai/php-ml/src/Clustering/KMeans/Point.php';
require_once 'php-ai/php-ml/src/Clustering/KMeans/Cluster.php';
require_once 'php-ai/php-ml/src/Clustering/KMeans/Space.php';
require_once 'php-ai/php-ml/src/Clustering/Clusterer.php';
require_once 'php-ai/php-ml/src/Clustering/KMeans.php';




/*  
$accidents_query = "SELECT * FROM accidents_view";
$accidents =mysqli_query($con,$accidents_query);
$key=0;
$locations=[];
while ($row = mysqli_fetch_array($accidents))
{
	array_push($locations,[$row['latitude'],$row['longitude']]);
}

//$samples = [[1, 1], [8, 7], [1, 2], [7, 8], [2, 1], [8, 9]];
$kmeans = new KMeans(4);
$kmeans->cluster($locations);
$results = $kmeans->cluster($locations);
foreach ($results as $key=>$result)
{
  $query = "INSERT INTO clustering SET clustering_latitude='".$result[0][0]."',clustering_longitude='".$result[0][1]."'";
	mysqli_query($con,$query);
  echo $query;
}

    	exit();




  $samples = [];
  $labels = [];

  $accidents_query = "SELECT * FROM accidents_view";
  $accidents =mysqli_query($con,$accidents_query);
  $key=0;

  while ($row = mysqli_fetch_array($accidents))
  {
	$month = explode("-",$row['Accident_date'])[1];
	array_push($samples,[$month,$row['victim_age'],$row['longitude'],$row['latitude']]);
	array_push($labels,$row['idVictim']);
	$key++;
  }

		$classifier = new KNearestNeighbors();
    	
    	$classifier->train($samples, $labels);
    	
    	$predict_key = $classifier->predict([10,28,35.3354,32.222154]);

    	$get_statistics = mysqli_query($con,"SELECT * FROM accidents_view WHERE idVictim=".$predict_key);
    	$statistics_result = mysqli_fetch_array($get_statistics);
    	echo $statistics_result['chief_complaint'];
    	
    	//echo $accidents[$predict_key]['chief_complaint'];
    	
exit();
*/

$response = array();

$days_query = "SELECT count(*) as total ,dw.name 
FROM accident_info 
LEFT JOIN day_week dw ON dw.idDayWeek= accident_info.Day_of_Week 
GROUP BY Day_of_Week";

$days_statistics = mysqli_query($con,$days_query);

$response["days_stat"] = array();
$total = 0;

while($row = mysqli_fetch_array($days_statistics))
{
    $days_stat = array();
    $days_stat['total'] = $row['total'];
    $total+=$row['total'];
    $days_stat['name'] = $row['name'];
    array_push($response["days_stat"], $days_stat);
}


$response['total']=$total;

$light_query = "SELECT count(*) as total , lc.name";
$light_query .= " FROM accident_info ai";
$light_query .= " LEFT JOIN light_condition lc ON lc.idLight=ai.Light_condition";
$light_query .= " GROUP BY ai.Light_condition";

$light_statistics = mysqli_query($con,$light_query);

$response['light_stat']=array();

while($row = mysqli_fetch_array($light_statistics))
{
    $light_stat = array();
    $light_stat['total'] = $row['total'];
    $light_stat['name'] = $row['name'];
    array_push($response["light_stat"], $light_stat);
}


$age_query = "SELECT v.victim_age as age,count(*) as total FROM `victim` v WHERE `victim_age`!='' GROUP BY v.victim_age";

$age_stats = mysqli_query($con,$age_query);

$btw_0_18 = 0;
$btw_19_30=0;
$btw_31_60=0;
$greater_60=0;

while($row = mysqli_fetch_array($age_stats))
{
  $age = $row['age'];
  $total = $row['total'];

  if($age>=0 && $age<=18)
  {
      $btw_0_18+=$total;
  }
  else if($age>=19 && $age<=30)
  {
    $btw_19_30 +=$total;
  }
  else if($age>=31 && $age<=60)
  {
    $btw_31_60+=$total;
  }
  else if($age>60)
  {
    $greater_60+=$total;
  }

}

$response['btw_0_18']=$btw_0_18;
$response['btw_19_30']=$btw_19_30;
$response['btw_31_60']=$btw_31_60;
$response['greater_60']=$greater_60;











echo json_encode($response);

?>