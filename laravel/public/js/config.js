function alert(title = '' , body = '')
{
    $('#testAlert').simpleAlert({
        title:title,
        message: body
    });
}

function alertCallback(title,body,success,failure)
{
    
    $('#testConfirm').simpleAlertCallback({
        title: title,
        message: body,
        success:success,
        cancel: failure
    });
}

function confirm(title = '' , body = '' , success , failure)
{
    $('#testConfirm').simpleConfirm({
        title: title,
        message: body,
        success:success,
        cancel: failure
    });
}

function prompt(title='' , message = '' ,type, value, success)
{
    $('#testPrompt').simplePrompt({
        title:title,
        message: "",
        type:type,
        value:value,
        success: success
    });

}

function startLoading(text,reference="content")
{
    $("#"+reference).waitMe({
            effect: 'roundBounce',
            text: text,
            bg: 'rgba(255,255,255,0.7)',
            color: '#083b66',
            fontWeight:'bold',
            onClose: function() {}
    });
}

function stopLoading(reference="content")
{
    $("#"+reference).waitMe('hide');
}

function request(link,data = '' , callback, loading = '')
{

    $.ajax({
        type:"POST",
        url:link,
        data:data,
        dataType: "json",
        cache:false,
        beforeSend: function()
        {
            startLoading(loading);
        },
        complete: function(){
            stopLoading();
        },
        success:function(data)
        {
            callback(data);
        },
        error:function(data)
        {
            alert("Unable to connect to server","","error");
        }
    });
}

