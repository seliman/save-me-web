<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/','LoginsController');

Route::resource('/accidents','AccidentsController');

Route::resource('/statistics','StatisticsController');

Route::resource('/users','UsersController');

Route::resource('/logout','LogoutsController');

Route::resource('/profile','ProfilesController');	

Route::resource('/edit/{accident_id}','EditsController');


	